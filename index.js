// 1. spaghetti code
// 2. bracket literal ( {} )
// 3. encapsulation
// 4. studentOne.method()
// 5. false
// 6. key:value
// 7. true
// 8. true
// 9. true
// 10. true


let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [90, 91, 92, 99],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s grades are :`, this.grades);
    },
    computeAve(){
        return this.grades.reduce((a,b) =>{
            return a+b
        })/(this.grades.length);
    },
    willPass(){
        return this.computeAve() >=85;
    },
    willPassWithHonor(){
        if(this.computeAve()>=90 && this.computeAve()<=100){
            return true;
        } else if(this.computeAve()>=85 && this.computeAve()<90){
            return false;
        } else {
            return undefined;
        }
    }
}

let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [91, 95, 96, 97],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s grades are :`, this.grades);
    },
    computeAve(){
        return this.grades.reduce((a,b) =>{
            return a+b
        })/(this.grades.length);
    },
    willPass(){
        return this.computeAve() >=85;
    },
    willPassWithHonor(){
        if(this.computeAve()>=90 && this.computeAve()<=100){
            return true;
        } else if(this.computeAve()>=85 && this.computeAve()<90){
            return false;
        } else {
            return undefined;
        }
    }
}

let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [71, 77, 70, 80],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s grades are :`, this.grades);
    },
    computeAve(){
        return this.grades.reduce((a,b) =>{
            return a+b
        })/(this.grades.length);
    },
    willPass(){
        return this.computeAve() >=85;
    },
    willPassWithHonor(){
        if(this.computeAve()>=90 && this.computeAve()<=100){
            return true;
        } else if(this.computeAve()>=85 && this.computeAve()<90){
            return false;
        } else {
            return undefined;
        }
    }
}

let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [88, 85, 86, 89],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s grades are :`, this.grades);
    },
    computeAve(){
        return this.grades.reduce((a,b) =>{
            return a+b
        })/(this.grades.length);
    },
    willPass(){
        return this.computeAve() >=85;
    },
    willPassWithHonor(){
        if(this.computeAve()>=90 && this.computeAve()<=100){
            return true;
        } else if(this.computeAve()>=85 && this.computeAve()<90){
            return false;
        } else {
            return undefined;
        }
    }
}

const classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents(){
        return this.students.filter(student => {
            return student.willPassWithHonor()===true;
        }).length;
    },
    honorsPercentage(){
        return`${(this.countHonorStudents()/this.students.length)*100}%`;
    },
    retrieveHonorStudentInfo(){
        let honorStudents = [];
        this.students.forEach(student =>{
            if(student.willPassWithHonor()===true){
                let honorStudent = {
                    aveGrade: student.computeAve(),
                    email: student.email,
                }
                honorStudents.push(honorStudent);
            }
        })
        return honorStudents;
    },
    sortHonorStudentsByGradeDesc(){
        return this.retrieveHonorStudentInfo().sort((previousStudent,currentStudent) => {
            return currentStudent.aveGrade - previousStudent.aveGrade;
        });
    }
}



//Function COding